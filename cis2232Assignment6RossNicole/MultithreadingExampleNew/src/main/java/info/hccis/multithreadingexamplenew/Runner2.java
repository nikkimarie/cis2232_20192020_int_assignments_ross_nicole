package info.hccis.multithreadingexamplenew;

import javax.swing.JOptionPane;

/**
 * This class can be used as a thread as it implements the Runnable interface.
 *
 * @author bjmaclean
 * @since 2017-12-05
 */
public class Runner2 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            RunnerBO.incrementCounter();
            String showInputDialog = JOptionPane.showInputDialog("Hello from runner2 " + i);
            try {
                Thread.sleep(400);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted");
            }
        }
        System.out.println("Runner 2 is finished, counter is equal to: "+RunnerBO.getCounter());
    }
}
