package info.hccis.camper.model.jpa;

import java.io.Serializable;

/**
 *
 * @author bjmac
 */
public class Ojtreflection implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private int studentId;
    private String studentName="unknown";
    private String reflection="unknown";

    public Ojtreflection() {
    }

    public Ojtreflection(int id) {
        this.id = id;
    }

    public Ojtreflection(int id, int studentId) {
        this.id = id;
        this.studentId = studentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInt() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getReflection() {
        return reflection;
    }

    public void setReflection(String reflection) {
        this.reflection = reflection;
    }
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }

//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Ojtreflection)) {
//            return false;
//        }
//        Ojtreflection other = (Ojtreflection) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "Ojtreflection{" + "id=" + id + ", studentId=" + studentId + ", studentName=" + studentName + ", reflection=" + reflection + '}';
    }


    
}
