package info.hccis.ojtreflection.entity.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Assignment 3 Web Application
 * @author Nicole Ross
 * @since 2020-06-04
 */
@Entity
@Table(name = "ojtreflection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ojtreflection.findAll", query = "SELECT o FROM Ojtreflection o"),
    @NamedQuery(name = "Ojtreflection.findByStudentId", query = "SELECT o FROM Ojtreflection o WHERE o.studentId = :studentId"),
    @NamedQuery(name = "Ojtreflection.findByStudentName", query = "SELECT o FROM Ojtreflection o WHERE o.studentName = :studentName"),
    @NamedQuery(name = "Ojtreflection.findByReflectionText", query = "SELECT o FROM Ojtreflection o WHERE o.reflectionText = :reflectionText")})
public class Ojtreflection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "studentId")
    private Integer studentId;
    @Size(max = 100)
    @Column(name = "studentName")
    private String studentName;
    @Size(max = 100)
    @Column(name = "reflectionText")
    private String reflectionText;

    public Ojtreflection() {
    }

    public Ojtreflection(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getReflectionText() {
        return reflectionText;
    }

    public void setReflectionText(String reflectionText) {
        this.reflectionText = reflectionText;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentId != null ? studentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ojtreflection)) {
            return false;
        }
        Ojtreflection other = (Ojtreflection) object;
        if ((this.studentId == null && other.studentId != null) || (this.studentId != null && !this.studentId.equals(other.studentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.ojtreflection.entity.jpa.Ojtreflection[ studentId=" + studentId + " ]";
    }

}
