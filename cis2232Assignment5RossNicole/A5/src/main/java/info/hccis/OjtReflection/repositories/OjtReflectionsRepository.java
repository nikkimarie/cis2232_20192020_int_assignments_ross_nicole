package info.hccis.OjtReflection.repositories;

import info.hccis.OjtReflection.entity.jpa.Ojtreflection;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OjtReflectionsRepository extends CrudRepository<Ojtreflection, Integer> {
        ArrayList<Ojtreflection> findAllByStudentName(String studentName);
}