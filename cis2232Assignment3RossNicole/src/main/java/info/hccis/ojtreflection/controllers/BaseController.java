package info.hccis.ojtreflection.controllers;

import info.hccis.ojtreflection.util.CisUtility;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {

    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
