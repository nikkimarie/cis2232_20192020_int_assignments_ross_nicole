package info.hccis.ojtreflection.bo;

import info.hccis.ojtreflection.util.CisUtility;

/**
 * @Description Assignment 1
 * @author Nicole Ross
 * @since May 21 2020
 */
public class OjtReflection {

    private int studentId;
    private String name;
    private String reflectionText;
    private static int nextId = 0;

    /**
     * @Description Assignment 1 default constructor
     * @author Nicole Ross
     * @since May 21 2020
     */
    public OjtReflection() {
    }

    /**
     * @Description Assignment 1 get information from user
     * @author Nicole Ross
     * @since May 21 2020
     */
    public void getInformation() {
        name = CisUtility.getInputString("Enter student name: ");
        reflectionText = CisUtility.getInputString("Enter reflection comments: ");

    }

    /**
     * @return 
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public int setNextId() {
        studentId = ++nextId;
        return studentId;
    }

    /**
     * @return 
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public static int getNextId() {
        return nextId;
    }

    /**
     * @param nextId
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public static void setNextId(int nextId) {
        OjtReflection.nextId = nextId;
    }

    /**
     * @return 
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * @param studentId
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * @return 
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return 
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public String getReflectionText() {
        return reflectionText;
    }

    /**
     * @param reflectionText
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    public void setReflectionText(String reflectionText) {
        this.reflectionText = reflectionText;
    }

    /**
     * @Description Assignment 1
     * @author Nicole Ross
     * @since May 21 2020
     */
    @Override
    public String toString() {
        return "Student ID: " + studentId + " Name: " + name + " Reflection Text: " + reflectionText;
    }

}
