package info.hccis.ojtreflection.bo;

import info.hccis.ojtreflection.dao.OjtReflectionDAO;
import info.hccis.ojtreflection.entity.jpa.Ojtreflection;

/**
 * @Assignment 3 Web Application
 * @author Nicole Ross
 * @since 2020-06-04
 */
public class OjtReflectionBO {

    public static void addReflection(Ojtreflection reflection) {

        System.out.println("Reflection object about to be added to the database"
                + ""
                + "\n" + reflection.toString());

        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();

        if (reflection.getStudentId() == 0) {
            reflectionDAO.insert(reflection);
        } else {
            reflectionDAO.update(reflection);
        }

    }

}
