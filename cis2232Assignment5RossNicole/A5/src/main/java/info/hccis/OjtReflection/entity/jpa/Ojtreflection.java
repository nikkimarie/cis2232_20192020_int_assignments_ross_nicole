package info.hccis.OjtReflection.entity.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Assignment 4 Spring Web Application (Part 2)
 * @author Nicole Ross
 * @since 2020-06-09
 */
@Entity
@Table(name = "ojtreflection")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ojtreflection.findAll", query = "SELECT o FROM Ojtreflection o"),
    @NamedQuery(name = "Ojtreflection.findById", query = "SELECT o FROM Ojtreflection o WHERE o.id = :id"),
    @NamedQuery(name = "Ojtreflection.findByStudentId", query = "SELECT o FROM Ojtreflection o WHERE o.studentId = :studentId"),
    @NamedQuery(name = "Ojtreflection.findByStudentName", query = "SELECT o FROM Ojtreflection o WHERE o.studentName = :studentName"),
    @NamedQuery(name = "Ojtreflection.findByReflection", query = "SELECT o FROM Ojtreflection o WHERE o.reflection = :reflection")})
public class Ojtreflection implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "studentId")
    private int studentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "studentName")
    private String studentName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 140)
    @Column(name = "reflection")
    private String reflection;

    public Ojtreflection() {
    }

    public Ojtreflection(Integer id) {
        this.id = id;
    }

    public Ojtreflection(Integer id, int studentId, String studentName, String reflection) {
        this.id = id;
        this.studentId = studentId;
        this.studentName = studentName;
        this.reflection = reflection;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getReflection() {
        return reflection;
    }

    public void setReflection(String reflection) {
        this.reflection = reflection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Ojtreflection)) {
            return false;
        }
        Ojtreflection other = (Ojtreflection) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.teetimebooker.entity.jpa.Ojtreflection[ id=" + id + " ]";
    }

}
