package info.hccis.OjtReflection.dao;

import info.hccis.OjtReflection.entity.jpa.Ojtreflection;
import info.hccis.OjtReflection.util.CisUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Assignment 4 Spring Web Application (Part 2)
 * @author Nicole Ross
 * @since 2020-06-09
 */
public class OjtReflectionDAO {

    private String userName = null, password = null, connectionString = null;
    private Connection conn = null;

    public OjtReflectionDAO() {
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(OjtReflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @param ojtReflection
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    public void insert(Ojtreflection ojtReflection) {
        try {
            String theStatement = "INSERT INTO Ojtreflection(id, studentId, studentName, reflection"
                    + "VALUES (0,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, ojtReflection.getStudentId());
            stmt.setString(2, ojtReflection.getStudentName());
            stmt.setString(3, ojtReflection.getReflection());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @param ojtReflection
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    public void update(Ojtreflection ojtReflection) {
        try {
            String theStatement = "UPDATE Ojtreflection SET studentId=?,studentName=?,reflection=? WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, ojtReflection.getStudentId());
            stmt.setString(2, ojtReflection.getStudentName());
            stmt.setString(3, ojtReflection.getReflection());
            stmt.setInt(4, ojtReflection.getId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @param id
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    public void delete(Integer id) {
        try {
            String theStatement = "DELETE FROM Ojtreflection WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @return 
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    public ArrayList<Ojtreflection> selectAll() {
        ArrayList<Ojtreflection> ojtReflections = new ArrayList();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Ojtreflection");

            while (rs.next()) {

                Integer id = rs.getInt("id");
                int studentId = rs.getInt("studentId");
                String studentName = rs.getString("studentName");
                String reflection = rs.getString("reflection");
                Ojtreflection ojtReflection = new Ojtreflection(id);
                ojtReflection.setStudentId(studentId);
                ojtReflection.setStudentName(studentName);
                ojtReflection.setReflection(reflection);
                ojtReflections.add(ojtReflection);
                System.out.println("Reflection for id: " + rs.getString("id") + " is " + rs.getString("studentName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting Ojtreflection. (" + ex.getMessage() + ")");

        }
        return ojtReflections;

    }

    /**
     * @param id
     * @return 
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    public Ojtreflection select(Integer id) {
        ArrayList<Ojtreflection> ojtReflections = new ArrayList();
        Ojtreflection ojtReflection = null;

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Ojtreflection where id=" + id);

            while (rs.next()) {

                int studentId = rs.getInt("studentId");
                String studentName = rs.getString("studentName");
                String reflection = rs.getString("reflection");
                ojtReflection = new Ojtreflection(id);
                ojtReflection.setStudentId(studentId);
                ojtReflection.setStudentName(studentName);
                ojtReflection.setReflection(reflection);

                ojtReflections.add(ojtReflection);
                System.out.println("Ojtreflection for id: " + rs.getString("id") + " is " + rs.getString("studentName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting reflections. (" + ex.getMessage() + ")");
        }
        return ojtReflection;

    }

}
