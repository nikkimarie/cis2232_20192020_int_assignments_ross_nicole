package com.mycompany.cis2232assignment1rossnicole;

import com.google.gson.Gson;
import info.hccis.ojtreflection.util.CisUtility;
import info.hccis.ojtreflection.bo.OjtReflection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Description Assignment 1
 * @author Nicole Ross
 * @since May 21 2020
 * @Requirement 1: When all the entries are added to an ArrayList OjtReflection
 * the details should be written to a file (c:\cis2232\reflection.json). This
 * can happen when the user exits the program. reflection should be encoded as
 * json and written to the file on a line per reflection.
 */
public class Controller {

    public static final String EXIT = "X";
    public static final String PATH = "c:\\cis2232\\";
    public static final String FILE_NAME = "reflection.json";
    private static int reflectionCounter;

    private static ArrayList<OjtReflection> reflection = new ArrayList();

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a Reflection\n"
            + "- S-Show Reflections\n"
            + "- U-Update Reflections\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        setupFile();

        loadReflections(reflection);

        String option = "";

        CisUtility.display("OJT Reflections");
        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }

    /**
     * @Description Assignment 1 menu
     * @author Nicole Ross
     * @since May 21 2020
     */
    public static void processMenuOption(String option) {
        switch (option.toUpperCase()) {
            case "A":
                CisUtility.display("Add a reflection");
                addReflection();
                break;
            case "S":
                CisUtility.display("Here are the reflections");
                showReflection(reflection);
                break;
            case "U":
                CisUtility.display("Update picked");
                updateRefelction(reflection);
                break;
            case "X":
                CisUtility.display("GoodBye!!");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }

    /**
     * @Description Assignment 1 Creating the file
     * @author Nicole Ross
     * @since May 21 2020
     * @Requirement 2: When the program starts, if the file created above exists
     * it should be used to load the ArrayList OjtReflection. This will
     * eliminate the need for the program to prompt for the attributes each time
     * the program is run.
     */
    public static void setupFile() {
        File myFile;
        try {
            Path path = Paths.get(PATH);
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                CisUtility.display("Error --------------> creating directory");
            }

            myFile = new File(PATH + FILE_NAME);
            if (myFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException ex) {
            CisUtility.display("Error ----------> creating new file");
        }
    }

    /**
     * @Description Assignment 1 adding a reflection
     * @author Nicole Ross
     * @since May 21 2020
     * @Requirement 1: When the user selects ‘A’ (for Add) - allow the user to
     * enter the student id, name, and reflection text. No validation is
     * required and the user can be expected to enter the right values. An
     * object oriented solution is expected. An OjtReflection class should be
     * used to contain the attributes.
     */
    public static void addReflection() {
        BufferedWriter writer = null;
        try {
            OjtReflection reflections = new OjtReflection();
            reflections.setNextId();

            reflections.getInformation();

            writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, true));

            Gson gson = new Gson();
            String reflectionJson = gson.toJson(reflections);

            writer.write(reflectionJson + System.lineSeparator());

            writer.close();
        } catch (IOException ex) {
            CisUtility.display("Error -------> accessing file");
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                CisUtility.display("Error ---------> closing file");
            }
        }
    }

    /**
     * @Description Assignment 1 showing all reflections
     * @author Nicole Ross
     * @since May 21 2020
     * @Requirement 3: Show all of the reflections details to the console. As
     * well at the top (before the list of reflections), it should show the
     * total number of reflections (a count of all of the reflections).
     */
    public static void showReflection(ArrayList<OjtReflection> reflection) {

        reflection.clear();
        loadReflections(reflection);
        reflectionCounter = reflection.size();
        System.out.println("The number of reflections in file: " + reflectionCounter);
        for (OjtReflection current : reflection) {
            CisUtility.display(current.toString());
        }

    }

    /**
     * @Description Assignment 1 Loading the Reflections
     * @author Nicole Ross
     * @since May 21 2020
     */
    public static void loadReflections(ArrayList<OjtReflection> reflection) {

        reflection.clear();
        try {
            Path reflectionPath = Paths.get(PATH + FILE_NAME);
            List<String> lines = Files.readAllLines(reflectionPath);
            Gson gson = new Gson();
            for (String current : lines) {
                if (!current.isEmpty()) {
                    OjtReflection reflections = gson.fromJson(current, OjtReflection.class);
                    reflection.add(reflections);
                    if (reflections.getStudentId() > OjtReflection.getNextId()) {
                        OjtReflection.setNextId(reflections.getStudentId());
                    }
                }
            }
        } catch (IOException ex) {
            CisUtility.display("Exception reading reflections from file");
        }

    }

    /**
     * @Description Assignment 1 ability to update a reflection
     * @author Nicole Ross
     * @since May 21 2020
     * @Requirement 4: Allow the user to update the reflection for a student in
     * the ArrayList created above. After updating, show all the reflections
     * again to the user.
     */
    public static void updateRefelction(ArrayList<OjtReflection> reflection) {

        loadReflections(reflection);

        int id = CisUtility.getInputInt("Enter the student id you want to update?");
        System.out.println("---------------------------------------------------------------");
        CisUtility.display("Please enter the changes you want to make");
        System.out.println("---------------------------------------------------------------");

        for (OjtReflection current : reflection) {
            if (current.getStudentId() == id) {
                current.getInformation();
                break;
            }
        }

        rewriteAllReflections(reflection);
    }

    /**
     * @Description Assignment 1 ability to rewrite reflections
     * @author Nicole Ross
     * @since May 21 2020
     */
    public static void rewriteAllReflections(ArrayList<OjtReflection> reflection) {

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, false));
        } catch (IOException ex) {
            Logger.getLogger(Controller.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        for (OjtReflection current : reflection) {
            try {
                Gson gson = new Gson();
                String reflectionJson = gson.toJson(current);

                writer.write(reflectionJson + System.lineSeparator());

            } catch (IOException ex) {
                CisUtility.display("Error ----> accessing file");
            }

        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }
}
