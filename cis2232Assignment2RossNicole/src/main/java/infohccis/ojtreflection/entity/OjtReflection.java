
package infohccis.ojtreflection.entity;

import info.hccis.ojtreflection.util.CisUtility;

/**
 * @Description Assignment 2 database
 * @author Nicole Ross
 * @since May 25 2020
 */
public class OjtReflection {

    private int studentId;
    private String studentName;
    private String reflectionText;
    private static int nextId = 0;

    /**
     * @Description Assignment 2 default constructor
     * @author Nicole Ross
     * @since May 25 2020
     */
    public OjtReflection() {
    }

    public OjtReflection(int studentId, String studentName, String reflectionText) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.reflectionText = reflectionText;
    }

    /**
     * @Description Assignment 2 get information from user
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void getInformation() {
        studentName = CisUtility.getInputString("Enter student's name: ");
        reflectionText = CisUtility.getInputString("Enter reflection comments: ");

    }

    /**
     * @return @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public int setNextId() {
        studentId = ++nextId;
        return studentId;
    }

    /**
     * @return @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public static int getNextId() {
        return nextId;
    }

    /**
     * @param nextId
     * @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public static void setNextId(int nextId) {
        OjtReflection.nextId = nextId;
    }

    /**
     * @return @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * @param studentId
     * @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * @return @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName
     * @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @return @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public String getReflectionText() {
        return reflectionText;
    }

    /**
     * @param reflectionText
     * @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void setReflectionText(String reflectionText) {
        this.reflectionText = reflectionText;
    }

    /**
     * @Description Assignment 2
     * @author Nicole Ross
     * @since May 25 2020
     */
    @Override
    public String toString() {
        return "Student ID: " + studentId + " Name: " + studentName + " Reflection Text: " + reflectionText;
    }

}
