package info.hccis.ojtreflection.rest;

import com.google.gson.Gson;
import info.hccis.OjtReflection.dao.OjtReflectionDAO;
import info.hccis.OjtReflection.entity.jpa.Ojtreflection;
import info.hccis.OjtReflection.repositories.OjtReflectionsRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/OjtReflectionService/ojtReflections")
public class OjtReflectionService {

    private static Map<Integer, Ojtreflection> DB = new HashMap<>();
    private final OjtReflectionsRepository br;

    @Autowired
    public OjtReflectionService(OjtReflectionsRepository br) {
        this.br = br;
    }

    @GET
    @Produces("application/json")
    public ArrayList<Ojtreflection> getAllReflections() {
        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) br.findAll();
        return ojtReflections;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam("id") int id) throws URISyntaxException 
    {

        Optional<Ojtreflection> ojtReflection = br.findById(id);

        if(ojtReflection == null) {
            return Response.status(404).build();
        }
        return Response
                .status(200)
                .entity(ojtReflection).build();
    }

    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createReflection(String ojtReflectionJson) 
    {
        
        Gson gson = new Gson();
        Ojtreflection ojtReflection = gson.fromJson(ojtReflectionJson, Ojtreflection.class);
        
        if(ojtReflection.getStudentName()== null || ojtReflection.getStudentName().isEmpty()) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }
 
        if(ojtReflection.getId() == null){
            ojtReflection.setId(0);
        }

        ojtReflection = br.save(ojtReflection);
        OjtReflectionDAO ojtReflectionDAO = new OjtReflectionDAO();
        ojtReflectionDAO.insert(ojtReflection);

        String temp = "";
        temp = gson.toJson(ojtReflection);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
    }
    
    @DELETE
    @Path("/{id}")
    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
        Optional<Ojtreflection> ojtReflection = br.findById(id);
        if(ojtReflection != null) {
            br.deleteById(id);
            return Response.status(HttpURLConnection.HTTP_CREATED).build();
        }
        return Response.status(404).build();
    }

    
// 
//    @GET
//    @Path("/{id}")
//    @Produces("application/json")
//    public Response getUserById(@PathParam("id") int id) throws URISyntaxException 
//    {
//        User user = DB.get(id);
//        if(user == null) {
//            return Response.status(404).build();
//        }
//        return Response
//                .status(200)
//                .entity(user)
//                .contentLocation(new URI("/user-management/"+id)).build();
//    }
// 
//    @PUT
//    @Path("/{id}")
//    @Consumes("application/json")
//    @Produces("application/json")
//    public Response updateUser(@PathParam("id") int id, User user) throws URISyntaxException 
//    {
//        User temp = DB.get(id);
//        if(user == null) {
//            return Response.status(404).build();
//        }
//        temp.setFirstName(user.getFirstName());
//        temp.setLastName(user.getLastName());
//        DB.put(temp.getId(), temp);
//        return Response.status(200).entity(temp).build();
//    }
// 
//     
//    static
//    {
//        User user1 = new User();
//        user1.setId(1);
//        user1.setFirstName("John");
//        user1.setLastName("Wick");
//        user1.setUri("/user-management/1");
// 
//        User user2 = new User();
//        user2.setId(2);
//        user2.setFirstName("Harry");
//        user2.setLastName("Potter");
//        user2.setUri("/user-management/2");
//         
//        DB.put(user1.getId(), user1);
//        DB.put(user2.getId(), user2);
//    }
//}
//Users.java
//
//package com.howtodoinjava.jerseydemo;
//  
//import java.util.ArrayList;
// 
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//  
//@XmlAccessorType(XmlAccessType.NONE)
//@XmlRootElement(name = "users")
//public class Users {
//  
//    @XmlElement(name="user")
//    private ArrayList<User> users;
//  
//    public ArrayList<User> getUsers() {
//        return users;
//    }
//  
//    public void setUsers(ArrayList<User> users) {
//        this.users = users;
//    }
}
