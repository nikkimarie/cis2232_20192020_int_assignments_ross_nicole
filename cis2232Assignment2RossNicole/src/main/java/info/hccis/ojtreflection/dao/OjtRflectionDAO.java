package info.hccis.ojtreflection.dao;

import infohccis.ojtreflection.entity.OjtReflection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Description Assignment 2 database DAO
 * @author Nicole Ross
 * @since May 25 2020
 */
public class OjtRflectionDAO {

    /**
     * @param reflection
     * @Description Assignment 2 database insert
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void insert(OjtReflection reflection) {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "root",
                    "");
        } catch (SQLException ex) {
            Logger.getLogger(OjtRflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            String theStatement = "INSERT INTO OjtReflection(studentId, studentName, reflectionText)"
                    + "VALUES (0,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, reflection.getStudentName());
            stmt.setString(2, reflection.getReflectionText());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @param reflection
     * @Description Assignment 2 database update file
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void update(OjtReflection reflection) {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "root",
                    "");
        } catch (SQLException ex) {
            Logger.getLogger(OjtRflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            String theStatement = "UPDATE INTO OjtReflection(studentId, studentName, reflectionText)"
                    + "VALUES (0,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, reflection.getStudentName());
            stmt.setString(2, reflection.getReflectionText());

            stmt.executeUpdate();

            conn.close();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @Description Assignment 2 database select all
     * @author Nicole Ross
     * @since May 25 2020
     */
    public ArrayList<OjtReflection> selectAll() {
        ArrayList<OjtReflection> reflection = new ArrayList();
        try {
            Connection conn = null;
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "root",
                    "");

            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select * from OjtReflection");

            while (rs.next()) {

                int studentId = rs.getInt("studentId");
                String studentName = rs.getString("studentName");
                String reflectionText = rs.getString("reflectionText");
                OjtReflection reflections = new OjtReflection(studentId, studentName, reflectionText);
                reflection.add(reflections);
                System.out.println("Reflection for student id: " + rs.getString("studentId") + " is " + rs.getString("studentName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting reflections. (" + ex.getMessage() + ")");

        }
        return reflection;

    }

}
