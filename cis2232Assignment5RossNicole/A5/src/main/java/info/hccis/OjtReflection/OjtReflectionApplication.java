package info.hccis.OjtReflection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OjtReflectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(OjtReflectionApplication.class, args);
	}

}
