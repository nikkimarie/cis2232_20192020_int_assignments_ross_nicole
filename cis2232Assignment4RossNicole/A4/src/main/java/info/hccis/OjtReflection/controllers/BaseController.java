package info.hccis.OjtReflection.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {
    @RequestMapping("/about")
    public String about() {
        return "other/about";
    }
}
