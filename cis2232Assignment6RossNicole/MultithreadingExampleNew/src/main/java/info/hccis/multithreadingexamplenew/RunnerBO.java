package info.hccis.multithreadingexamplenew;

/**
 * A class with a static attribute
 * @author bjm
 * @since 20200623
 */
public class RunnerBO {
    private static int counter;
    
    public static synchronized void incrementCounter(){
        try {
            int counterTemp = counter;
            Thread.sleep(100);
            counterTemp++;
            counter = counterTemp;
        } catch (InterruptedException ex) {
            System.out.println("Interupded");
        }
        
    }
    public static int getCounter(){
        return counter;
    }
}
