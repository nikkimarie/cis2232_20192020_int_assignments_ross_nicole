package info.hccis.ojtreflection.controllers;

import info.hccis.ojtreflection.bo.OjtReflectionBO;
import info.hccis.ojtreflection.dao.OjtReflectionDAO;
import info.hccis.ojtreflection.entity.jpa.Ojtreflection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Assignment 3 Web Application
 * @author Nicole Ross
 * @since 2020-06-04
 */
@Controller
@RequestMapping("/reflections")
public class OjtReflectionController {

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the bookings from the database.
        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();
        ArrayList<Ojtreflection> reflections = reflectionDAO.selectAll();
        model.addAttribute("reflections", reflections);

        return "reflections/list";
    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    @RequestMapping("/add")
    public String add(Model model) {

        Ojtreflection reflection = new Ojtreflection();
        reflection.setStudentId(0);

        model.addAttribute("reflection", reflection);

        return "reflections/add";
    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("studentId");
        int studentId = Integer.parseInt(idString);
        System.out.println("NRTEST - studentId=" + studentId);

        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();
        Ojtreflection selectedReflection = reflectionDAO.select(studentId);

        if (selectedReflection == null) {
            return "index";
        } else {
            model.addAttribute("reflection", selectedReflection);
            return "reflections/add";
        }
    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("studentId");
        int studentId = Integer.parseInt(idString);
        System.out.println("NRTEST - studentId=" + studentId);

        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();
        reflectionDAO.delete(studentId);
        ArrayList<Ojtreflection> reflections = reflectionDAO.selectAll();
        model.addAttribute("reflections", reflections);

        return "reflections/list";

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("reflection") Ojtreflection reflection, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "reflections/add";
        }

//        //Apply validation for the name and the comment.
        if (reflection.getStudentName().isEmpty()) {
            String message = "name is needed.";
            model.addAttribute("message", message);
            model.addAttribute("reflection", reflection);
            return "reflections/add";
        }
        if (reflection.getReflectionText().isEmpty()) {
            String message = "comment is needed.";
            model.addAttribute("message", message);
            model.addAttribute("reflection", reflection);
            return "reflections/add";
        }

        OjtReflectionBO.addReflection(reflection);

        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();
        ArrayList<Ojtreflection> reflections = reflectionDAO.selectAll();
        model.addAttribute("reflections", reflections);

        return "reflections/list";
    }

}
