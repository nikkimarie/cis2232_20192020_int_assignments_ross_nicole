package info.hccis.multithreadingexamplenew;

/**
 * This class will extend the thread class and can be used as a thread.
 *
 * @author bjmaclean
 * @since 2017-12-05
 */
public class Runner extends Thread {

    @Override
    public void run() {
       
        //I am just looping through, you could do whatever you like in this thread.
        //for example prompt the user for something to write to a file.
        
        for (int i = 0; i < 10; i++) {
            RunnerBO.incrementCounter();
            System.out.println("Hello from runner " + i);
            
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted");
            }
        }
        
        System.out.println("Runner 1 is finished, counter is equal to: "+RunnerBO.getCounter());
        
    }
}
