package info.hccis.multithreadingexamplenew;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Application which uses two threads
 *
 * @author bjm
 * @since 2020-06-23
 */
public class Controller {

    public static final String PATH = "d:\\threading\\";
    public static final String FILE_NAME = "threading.json";

    public static void main(String[] args) {
        Runner runner = new Runner();

        runner.start();

        Thread runner2 = new Thread(new Runner2());

        runner2.start();

        try {
            runner.join();
            runner2.join();
        } catch (InterruptedException ex) {
            System.out.println("Interupted");
        }

        System.out.println(
                "Main method is finished");

    }

}
