package info.hccis.OjtReflection.controllers;

import info.hccis.OjtReflection.entity.jpa.Ojtreflection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import info.hccis.OjtReflection.repositories.OjtReflectionsRepository;

/**
 * @Assignment 4 Spring Web Application (Part 2)
 * @author Nicole Ross
 * @since 2020-06-09
 */
@Controller
@RequestMapping("/ojtReflections")
public class OjtReflectionController {

    private final OjtReflectionsRepository reflectionRepository;

    public OjtReflectionController(OjtReflectionsRepository br) {
        reflectionRepository = br;
    }

    /**
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/list")
    public String list(Model model) {

        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) reflectionRepository.findAll();
        model.addAttribute("ojtReflections", ojtReflections);
        model.addAttribute("amountOfReflections", reflectionRepository.count());

        return "ojtReflections/list";
    }

    /**
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/add")
    public String add(Model model) {

        Ojtreflection ojtReflection = new Ojtreflection();
        ojtReflection.setId(0);
        model.addAttribute("ojtReflection", ojtReflection);

        return "ojtReflections/add";
    }

    /**
     * @param model
     * @return 
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/find")
    public String find(Model model) {

        Set<String> names = new HashSet();

        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) reflectionRepository.findAll();
        for (Ojtreflection current : ojtReflections) {
            names.add(current.getStudentName());
        }

        model.addAttribute("names", names);

        return "ojtReflections/find";
    }

    /**
     * @param model
     * @param request
     * @return 
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        Integer id = Integer.parseInt(idString);
        System.out.println("NRTEST - id=" + id);

        Optional<Ojtreflection> selectedReflection = reflectionRepository.findById(id);

        ArrayList<Ojtreflection> nicoleReflections = reflectionRepository.findAllByStudentName("Nicole Ross");
        System.out.println("NRTEST Nicole reflections: ");
        for (Ojtreflection current : nicoleReflections) {
            System.out.println(current.getStudentId() + " " + current.getReflection());
        }

        if (selectedReflection == null) {
            return "index";
        } else {
            model.addAttribute("ojtReflection", selectedReflection);
            return "ojtReflections/add";
        }
    }

    /**
     * @param model
     * @param request
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        Integer id = Integer.parseInt(idString);
        System.out.println("NRTEST - id=" + id);

        reflectionRepository.deleteById(id);

        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) reflectionRepository.findAll();
        model.addAttribute("ojtReflections", ojtReflections);
        model.addAttribute("amountOfReflections", reflectionRepository.count());

        return "ojtReflections/list";

    }

    /**
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     * @param model
     * @param ojtReflection
     * @param result
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("ojtReflection") Ojtreflection ojtReflection, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "ojtReflections/add";
        }
        reflectionRepository.save(ojtReflection);
        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) reflectionRepository.findAll();
        model.addAttribute("ojtReflections", ojtReflections);
        model.addAttribute("amountOfReflections", reflectionRepository.count());
        return "ojtReflections/list";
    }

    /**
     * @param model
     * @param ojtReflection
     * @return
     * @Assignment 4 Spring Web Application (Part 2)
     * @author Nicole Ross
     * @since 2020-06-09
     */
    @RequestMapping("/findSubmit")
    public String findSubmit(Model model, @ModelAttribute("ojtReflection") Ojtreflection ojtReflection) {

        System.out.println("NRTEST: about to find " + ojtReflection.getStudentName() + "'s ojtReflections");
        ArrayList<Ojtreflection> ojtReflections = (ArrayList<Ojtreflection>) reflectionRepository.findAllByStudentName(ojtReflection.getStudentName());
        model.addAttribute("ojtReflections", ojtReflections);

        model.addAttribute("findNameMessage", " (" + ojtReflection.getStudentName() + ")");
        model.addAttribute("amountOfReflections", reflectionRepository.count());

        return "ojtReflections/list";
    }

}
