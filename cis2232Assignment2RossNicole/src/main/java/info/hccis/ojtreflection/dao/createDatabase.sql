DROP DATABASE if exists ojt;
create database ojt;
use ojt;

grant select, insert, update, delete on ojt.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

CREATE TABLE IF NOT EXISTS OjtReflection (

studentId int(5) NOT NULL,

studentName varchar(20) NOT NULL,
reflectionText varchar(140) NOT NULL
);
ALTER TABLE OjtReflection
  ADD PRIMARY KEY (studentId);

ALTER TABLE OjtReflection
  MODIFY studentId int(5) NOT NULL AUTO_INCREMENT;
