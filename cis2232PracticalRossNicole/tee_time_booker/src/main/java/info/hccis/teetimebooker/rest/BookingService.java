package info.hccis.teetimebooker.rest;

import com.google.gson.Gson;
import info.hccis.teetimebooker.dao.BookingDAO;
import info.hccis.teetimebooker.entity.jpa.Booking;
import info.hccis.teetimebooker.repositories.BookingRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/BookingService/bookings")
public class BookingService {

    private static Map<Integer, Booking> DB = new HashMap<>();
    private final BookingRepository br;

    @Autowired
    public BookingService(BookingRepository br) {
        this.br = br;
    }
    //Create a new service which will return all the bookings on a given day. 

    @GET
    @Path("/{bookingDate}")
    @Produces("application/json")
    public ArrayList<Booking> getBookingByDate(@PathParam("bookingDate") String bookingDate) {
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAllByBookingDate(bookingDate);
        return bookings;
    }

    @GET
    @Produces("application/json")
    public ArrayList<Booking> getAllBookings() {
        ArrayList<Booking> bookings = (ArrayList<Booking>) br.findAll();
        return bookings;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Booking> booking = br.findById(id);

        if (booking == null) {
            return Response.status(404).build();
        }
        return Response
                .status(200)
                .entity(booking).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createBooking(String bookingJson) {

        Gson gson = new Gson();
        Booking booking = gson.fromJson(bookingJson, Booking.class);

        if (booking.getName1() == null || booking.getName1().isEmpty()) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }

        if (booking.getId() == null) {
            booking.setId(0);
        }

        booking = br.save(booking);
//        BookingDAO bookingDAO = new BookingDAO();
//        bookingDAO.insert(booking);

        String temp = "";
        temp = gson.toJson(booking);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
        Optional<Booking> booking = br.findById(id);
        if (booking != null) {
            br.deleteById(id);
            return Response.status(HttpURLConnection.HTTP_CREATED).build();
        }
        return Response.status(404).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateBooking(@PathParam("id") int id, String bookingJson) throws URISyntaxException {

        Gson gson = new Gson();
        Booking booking = gson.fromJson(bookingJson, Booking.class);

        if (booking.getName1() == null || booking.getName1().isEmpty()) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }

        if (booking.getId() == null) {
            booking.setId(0);
        }

        booking = br.save(booking);
//        BookingDAO bookingDAO = new BookingDAO();
//        bookingDAO.insert(booking);

        String temp = "";
        temp = gson.toJson(booking);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
// 
//    @GET
//    @Path("/{id}")
//    @Produces("application/json")
//    public Response getUserById(@PathParam("id") int id) throws URISyntaxException 
//    {
//        User user = DB.get(id);
//        if(user == null) {
//            return Response.status(404).build();
//        }
//        return Response
//                .status(200)
//                .entity(user)
//                .contentLocation(new URI("/user-management/"+id)).build();
//    }
// 

// 
//     
//    static
//    {
//        User user1 = new User();
//        user1.setId(1);
//        user1.setFirstName("John");
//        user1.setLastName("Wick");
//        user1.setUri("/user-management/1");
// 
//        User user2 = new User();
//        user2.setId(2);
//        user2.setFirstName("Harry");
//        user2.setLastName("Potter");
//        user2.setUri("/user-management/2");
//         
//        DB.put(user1.getId(), user1);
//        DB.put(user2.getId(), user2);
//    }
//}
//Users.java
//
//package com.howtodoinjava.jerseydemo;
//  
//import java.util.ArrayList;
// 
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//  
//@XmlAccessorType(XmlAccessType.NONE)
//@XmlRootElement(name = "users")
//public class Users {
//  
//    @XmlElement(name="user")
//    private ArrayList<User> users;
//  
//    public ArrayList<User> getUsers() {
//        return users;
//    }
//  
//    public void setUsers(ArrayList<User> users) {
//        this.users = users;
//    }
}
