package com.mycompany.cis2232assignment2rossnicole;

import info.hccis.ojtreflection.bo.OjtReflectionBO;

import info.hccis.ojtreflection.util.CisUtility;

/**
 * @Description Assignment 2 database
 * @author Nicole Ross
 * @since May 25 2020
 */
public class Controller {

    public static final String EXIT = "X";
    private static OjtReflectionBO reflectionBO = new OjtReflectionBO();

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a Reflection\n"
            + "- S-Show Reflections\n"
            + "- U-Update Reflections\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        reflectionBO.loadReflections();

        String option = "";

        CisUtility.display("OJT Reflections");
        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }

    /**
     * @Description Assignment 2 database
     * @author Nicole Ross
     * @since May 25 2020
     */
    public static void processMenuOption(String option) {
        switch (option.toUpperCase()) {
            case "A":
                CisUtility.display("Add a reflection");
                reflectionBO.addReflection();
                break;
            case "S":
                CisUtility.display("Here are the reflections");
                reflectionBO.showReflection();
                break;
            case "U":
                CisUtility.display("Update picked");
                reflectionBO.updateRefelction();
                break;
            case "X":
                CisUtility.display("GoodBye!!");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }

}
