package info.hccis.ojtreflection.dao;

import info.hccis.ojtreflection.entity.jpa.Ojtreflection;
import info.hccis.ojtreflection.util.CisUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Assignment 3 Web Application
 * @author Nicole Ross
 * @since 2020-06-04
 */
public class OjtReflectionDAO {

    private String userName = null, password = null, connectionString = null;
    private Connection conn = null;

    public OjtReflectionDAO() {
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("nrtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(OjtReflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    public void insert(Ojtreflection reflection) {

        try {
            String theStatement = "INSERT INTO OjtReflection(studentId, studentName, reflectionText)"
                    + "VALUES (0,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, reflection.getStudentName());
            stmt.setString(2, reflection.getReflectionText());
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    public void update(Ojtreflection reflection) {
        try {
            String theStatement = "UPDATE OjtReflection SET studentName=?,reflectionText=? WHERE studentId=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, reflection.getStudentName());
            stmt.setString(2, reflection.getReflectionText());
            stmt.setInt(3, reflection.getStudentId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    public void delete(int studentId) {
        try {
            String theStatement = "DELETE FROM OjtReflection WHERE studentId=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, studentId);
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    public ArrayList<Ojtreflection> selectAll() {
        ArrayList<Ojtreflection> reflections = new ArrayList();
        try {
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select * from OjtReflection");

            while (rs.next()) {

                int studentId = rs.getInt("studentId");
                String studentName = rs.getString("studentName");
                String reflectionText = rs.getString("reflectionText");
                Ojtreflection reflection = new Ojtreflection(studentId);
                reflection.setStudentName(studentName);
                reflection.setReflectionText(reflectionText);
                reflections.add(reflection);
                System.out.println("Reflection for student id: " + rs.getString("studentId") + " is " + rs.getString("studentName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting reflections. (" + ex.getMessage() + ")");

        }
        return reflections;

    }

    /**
     * @Assignment 3 Web Application
     * @author Nicole Ross
     * @since 2020-06-04
     */
    public Ojtreflection select(int studentId) {
        ArrayList<Ojtreflection> reflections = new ArrayList();
        Ojtreflection reflection = null;

        try {
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select * from OjtReflection where studentId=" + studentId);

            while (rs.next()) {

                String studentName = rs.getString("studentName");
                String reflectionText = rs.getString("reflectionText");
                reflection = new Ojtreflection(studentId);
                reflection.setStudentName(studentName);
                reflection.setReflectionText(reflectionText);
                reflections.add(reflection);
                System.out.println("Reflection for student id: " + rs.getString("studentId") + " is " + rs.getString("studentName"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting reflections. (" + ex.getMessage() + ")");
        }
        return reflection;

    }

}
