package info.hccis.OjtReflection.bo;

import info.hccis.OjtReflection.dao.OjtReflectionDAO;
import info.hccis.OjtReflection.entity.jpa.Ojtreflection;

/**
 * @Assignment 4 Spring Web Application (Part 2)
 * @author Nicole Ross
 * @since 2020-06-09
 */
public class OjtReflectionBO {

    public static void addReflection(Ojtreflection ojtReflection) {

        System.out.println("Reflection object about to be added to the database"
                + ""
                + "\n" + ojtReflection.toString());

        OjtReflectionDAO reflectionDAO = new OjtReflectionDAO();

        if (ojtReflection.getId() == 0) {
            reflectionDAO.insert(ojtReflection);
        } else {
            reflectionDAO.update(ojtReflection);
        }

    }

}
