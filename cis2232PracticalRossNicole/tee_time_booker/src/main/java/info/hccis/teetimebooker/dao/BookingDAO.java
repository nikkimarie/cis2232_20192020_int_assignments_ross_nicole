package info.hccis.teetimebooker.dao;

import info.hccis.teetimebooker.entity.jpa.Booking;
import info.hccis.teetimebooker.util.CisUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class with methods related to Booking Database class
 *
 * @author bjm modified by Nicole Ross
 * @since 20200630
 */
public class BookingDAO {

    private String userName = null, password = null, connectionString = null;
    private Connection conn = null;

    public BookingDAO() {
        String propFileName = "application";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        System.out.println("bjtest datasource:  " + rb.getString("spring.datasource.url"));
        connectionString = rb.getString("spring.datasource.url");
        userName = rb.getString("spring.datasource.username");
        password = rb.getString("spring.datasource.password");

        try {
            conn = DriverManager.getConnection(
                    connectionString, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Insert a booking into the database.
     *
     * @since 2020-06-30
     * @author BJM modified by Nicole Ross
     */
    public void insert(Booking booking) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "INSERT INTO Booking(id, name1, name2, name3, "
                    + "name4, bookingDate, bookingTime, courseName, createdDateTime, costPerPlayer) "
                    + "VALUES (0,?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, booking.getName1());
            stmt.setString(2, booking.getName2());
            stmt.setString(3, booking.getName3());
            stmt.setString(4, booking.getName4());
            stmt.setString(5, booking.getBookingDate());
            stmt.setString(6, booking.getBookingTime());
            stmt.setString(7, booking.getCourseName());
            stmt.setInt(8, booking.getCostPerPlayer());
            stmt.setString(9, CisUtility.getCurrentDate("yyyy-MM-dd hh:mm"));

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * Insert a booking into the database.
     *
     * @since 2020-06-30
     * @author BJM modified by Nicole Ross
     */
    public void update(Booking booking) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "UPDATE Booking SET name1=?,name2=?,name3=?,name4=?,bookingDate=?,bookingTime=?,courseName=?,createdDateTime=?, costPerPlayer=? WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, booking.getName1());
            stmt.setString(2, booking.getName2());
            stmt.setString(3, booking.getName3());
            stmt.setString(4, booking.getName4());
            stmt.setString(5, booking.getBookingDate());
            stmt.setString(6, booking.getBookingTime());
            stmt.setString(7, booking.getCourseName());
            stmt.setInt(8, booking.getCostPerPlayer());
            stmt.setString(9, CisUtility.getCurrentDate("yyyy-MM-dd hh:mm"));
            stmt.setInt(10, booking.getId());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * Delete a booking into the database. (Issue#2)
     *
     * @since 2020-06-30
     * @author BJM modified by Nicole Ross
     *
     */
    public void delete(int id) {

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "DELETE FROM booking WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * Select all bookings from the database
     *
     * @since 2020-06-30
     * @author BJM modified by Nicole Ross
     */
    public ArrayList<Booking> selectAll() {
        ArrayList<Booking> bookings = new ArrayList();
        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Booking");

            //Show all the bookers
            while (rs.next()) {

                int id = rs.getInt("id");
                String name1 = rs.getString("name1");
                String name2 = rs.getString("name2");
                String name3 = rs.getString("name3");
                String name4 = rs.getString("name4");
                String bookingDate = rs.getString("bookingDate");
                String bookingTime = rs.getString("bookingTime");
                String courseName = rs.getString("courseName");
                int costPerPlayer = rs.getInt("costPerPlayer");
                Booking booking = new Booking(id);
                booking.setName1(name1);
                booking.setName2(name2);
                booking.setName3(name3);
                booking.setName4(name4);
                booking.setCourseName(courseName);
                booking.setBookingDate(bookingDate);
                booking.setBookingTime(bookingTime);
                booking.setCostPerPlayer(costPerPlayer);
                bookings.add(booking);
                System.out.println("Booker for id: " + rs.getString("id") + " is " + rs.getString("name1"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");

        }
        return bookings;

    }

    /**
     * Select a booking from the database
     *
     * @since 2020-06-30
     * @author BJM modified by Nicole Ross
     */
    public Booking select(int id) {
        ArrayList<Booking> bookings = new ArrayList();
        Booking booking = null;

        try {
            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Booking where id=" + id);

            //Show all the bookers
            while (rs.next()) {

                String name1 = rs.getString("name1");
                String name2 = rs.getString("name2");
                String name3 = rs.getString("name3");
                String name4 = rs.getString("name4");
                String bookingDate = rs.getString("bookingDate");
                String bookingTime = rs.getString("bookingTime");
                String courseName = rs.getString("courseName");
                int costPerPlayer = rs.getInt("costPerPlayer");
                booking = new Booking(id);
                booking.setName1(name1);
                booking.setName2(name2);
                booking.setName3(name3);
                booking.setName4(name4);
                booking.setCourseName(courseName);
                booking.setBookingDate(bookingDate);
                booking.setBookingTime(bookingTime);
                booking.setCostPerPlayer(costPerPlayer);
                bookings.add(booking);
                System.out.println("Booker for id: " + rs.getString("id") + " is " + rs.getString("name1"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");
        }
        return booking;

    }

}
