package info.hccis.ojtreflection.bo;

import infohccis.ojtreflection.entity.OjtReflection;
import info.hccis.ojtreflection.dao.OjtRflectionDAO;
import info.hccis.ojtreflection.util.CisUtility;

import java.util.ArrayList;


/**
 * @Description Assignment 2 database
 * @author Nicole Ross
 * @since May 25 2020
 */
public class OjtReflectionBO {

    private static int reflectionCounter;
    private ArrayList<OjtReflection> reflection = new ArrayList();


    /**
     * @Description Assignment 2 database adding reflection
     * @author Nicole Ross
     * @since May 25 2020
     */
    public static void addReflection() {
        OjtReflection reflection = new OjtReflection();
        reflection.getInformation();

        OjtRflectionDAO reflectionDAO = new OjtRflectionDAO();
        reflectionDAO.insert(reflection);
    }

    /**
     * @Description Assignment 2 database showing reflection
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void showReflection() {

        reflection.clear();
        loadReflections();
        reflectionCounter = reflection.size();
        System.out.println("---------------------------------------------------------------");
        System.out.println("The number of reflections in file: " + reflectionCounter);
        System.out.println("---------------------------------------------------------------");
        for (OjtReflection current : reflection) {
            CisUtility.display(current.toString());
        }

    }

    /**
     * @Description Assignment 2 database Loading the Reflections
     * @author Nicole Ross
     * @since May 25 2020
     */
    public void loadReflections() {
        reflection.clear();
        OjtRflectionDAO reflectionDAO = new OjtRflectionDAO();
        reflection = reflectionDAO.selectAll();

    }

    /**
     * @Description Assignment 2 database ability to update a reflection
     * @author Nicole Ross
     * @since May 21 2020
     */
    public void updateRefelction() {

        loadReflections();

        int id = CisUtility.getInputInt("Enter the student id you want to update?");
        System.out.println("---------------------------------------------------------------");
        CisUtility.display("Please enter the changes you want to make");
        System.out.println("---------------------------------------------------------------");

        for (OjtReflection current : reflection) {
            if (current.getStudentId() == id) {
                current.getInformation();
                OjtRflectionDAO reflectionDAO = new OjtRflectionDAO();
                reflectionDAO.update(current);
                reflection = reflectionDAO.selectAll();
                break;
            }
        }
    }

}
