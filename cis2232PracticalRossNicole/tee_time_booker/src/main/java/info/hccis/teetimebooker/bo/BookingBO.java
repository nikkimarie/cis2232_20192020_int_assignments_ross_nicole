package info.hccis.teetimebooker.bo;

import info.hccis.teetimebooker.dao.BookingDAO;
import info.hccis.teetimebooker.entity.jpa.Booking;

/**
 * Business logic associated with bookings
 *
 * @author bjm modified: nicole ross
 * @since 2020-06-30
 */
public class BookingBO {

    //Daytime times before 15:00 cost $80 
    //Twilight Tee times after 15:00 cost $55 
    private static int dayTimeCost = 80;
    private static int nightTimeCost = 55;

    //Add a method to the BookingBO class which will accept the time of the 
    //booking and return the cost per player for that time.
    public void getCost(String time) {
        Booking booking = new Booking();
        if (booking.getBookingTime().equals("15:00")) {
            booking.setCostPerPlayer(nightTimeCost);

        } else {
            booking.setCostPerPlayer(dayTimeCost);
        }
    }

    public static void addBooking(Booking booking) {

        System.out.println("Booking object about to be added to the database"
                + ""
                + "\n" + booking.toString());

        //Add that booking to the database
        BookingDAO bookingDAO = new BookingDAO();

        if (booking.getId() == 0) {
            bookingDAO.insert(booking);
        } else {
            bookingDAO.update(booking);
        }

    }

}
