package info.hccis.teetimeboker;

import info.hccis.teetimebooker.entity.jpa.Booking;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//Create two unit tests which test the getCost method created above which assert
// the following two test cases the cost of a booking at 3 pm is 55. The cost of a booking at 7am is 80 
@SpringBootTest
class TeeTimeApplicationTests {

    @Test
    void contextLoads() {
    }
    private Booking bookingCost;

    TeeTimeApplicationTests() {
        bookingCost = new Booking();
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        bookingCost = new Booking();

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testSetCostOfBooking_3pm_55Dollars() {

        bookingCost.setBookingDate("15:00");
        bookingCost.setCostPerPlayer(55);
    }

    @Test
    public void testSetCostOfBooking_7am_80Dollars() {

        bookingCost.setBookingDate("07:00");
        bookingCost.setCostPerPlayer(80);
    }

}
